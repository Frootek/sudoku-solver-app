import tkinter as tk

from functools import partial
from solve_sudoku import solve_sudoku

from about_page import AboutPage

SMALL_FONT = ("Verdana", 10)


class SudokuGrid():
    def __init__(self,parent, game_engine):

        self.game_engine = game_engine

        self.btns = {}

        for j in range (0,9):
            for i in range (0,9):
                cell = tk.Button(parent, width=5, height=3,relief=tk.GROOVE,command = partial(self.button_click,j,i))
                cell.grid(row=j,column=i)
                self.btns[(i,j)] = cell


    def button_click(self,j,i):
        from pynput import keyboard

        with keyboard.Events() as events:
            for event in events:

                if event.key == keyboard.Key.esc or event.key.char == 'c' or  event.key.char=='0' :
                    self.btns[(i,j)].configure(text = '')
                    self.game_engine.update_square(j,i,0)
                    break
                else:
                    if  not event.key.char.isdigit():
                        break
                    
                    self.game_engine.update_square(j,i,int(event.key.char))
                    self.btns[(i,j)].configure(text = event.key.char)
                    break


    def update_grid(self):
        sudoku_grid = self.game_engine.get_sudoku()
        for j in range(9):
            for i in range(9):
                if(sudoku_grid[j][i] == 0):
                    self.btns[(i,j)].configure(text = '')
                else:
                    self.btns[(i,j)].configure(text = sudoku_grid[j][i])




class SolveBtn(tk.Button):
    def __init__(self,parent,game_engine, sudoku_grid):                   
        self.game_engine = game_engine
        self.sudoku_grid = sudoku_grid
        super().__init__(parent, text = 'Solve Sudoku', command = lambda : self.solve())



    def solve(self):
        try:
            solve_sudoku(self.game_engine.get_sudoku())
        except:
            pass

        self.sudoku_grid.update_grid()

class InputOnWebBtn(tk.Button):
    def __init__(self,parent,game_engine):                   
        self.game_engine = game_engine
        super().__init__(parent, text = 'Input on web', command = lambda : self.input_on_web())
        self.number_of_mouse_clicks = 0
        self.my_bbox = ()

    def get_square_positions(self,bbox):

        increment_y = ((bbox[3]-bbox[1]) / 9) 
        increment_x = ((bbox[2]-bbox[0]) / 9)

        square_position = {}

        x  = bbox[0] + increment_x/2
        y  = bbox[1] + increment_y/2

        for j in range(9):
            for i in range(9):
                square_position[(i,j)] = (x,y)
                x += increment_x
            y += increment_y
            x = bbox[0] + increment_x/2

        return square_position



    def on_click(self,x, y, button, pressed):
        if pressed:
            self.my_bbox += (x,y)
        self.number_of_mouse_clicks += 1
        if (self.number_of_mouse_clicks==4):
            return False

    def input_on_web(self):
        from pynput import mouse
        from pynput.mouse import Button
        from pynput import keyboard
        import time

        self.number_of_mouse_clicks = 0
        self.my_bbox = ()
        sudoku_grid = self.game_engine.get_sudoku()

        with mouse.Listener(on_click=self.on_click) as listener:
            try:
                listener.join()
            except :
                pass

        square_coordinates = self.get_square_positions(self.my_bbox)
        keyboard_controller = keyboard.Controller()

        mouse_controller = mouse.Controller()

        for j in range(9):
            for i in range(9):
                mouse_controller.position = square_coordinates[(i,j)]
                mouse_controller.press(Button.left)
                mouse_controller.release(Button.left)
                time.sleep(0.1)
                keyboard_controller.press(str(sudoku_grid[j][i]))
                keyboard_controller.release(str(sudoku_grid[j][i]))
                time.sleep(0.1)


class ClearGridBtn(tk.Button):
    def __init__(self,parent,game_engine, sudoku_grid):                   
        self.game_engine = game_engine
        self.sudoku_grid = sudoku_grid
        super().__init__(parent, text = 'Clear Sudoku', command = lambda : self.clear_sudoku()) 

    def clear_sudoku(self):
        self.game_engine.clear_sudoku()
        self.sudoku_grid.update_grid()



class AboutBtn(tk.Button):
    def __init__(self,parent,controler):
        super().__init__(parent, text = 'About', command = lambda : self.show_about_page())

    def show_about_page(self):
        new_window = tk.Toplevel()
        new_window.geometry("1100x650")
        
        about_page = AboutPage(new_window, self)
        about_page.pack(fill='both')

        new_window.mainloop()