
import tkinter as tk

from sudoku_solver_frame import SudokuSolverFrame

class App(tk.Tk):
    def __init__(self):
        tk.Tk.__init__(self)
        super().title("Sudoku-Solver by Frootek")
        
        self.geometry("415x670")

        self.frames = {}

        #add StartPage and MineSweeper to container
        frame = SudokuSolverFrame(self,self)
        self.frames["SudokuSolverFrame"] = frame
        frame.pack(fill='both')

        self.show_frame("SudokuSolverFrame")


    def show_frame(self, cont):
        frame = self.frames[cont]
        frame.tkraise()

app = App()
app.resizable(False, False)
app.mainloop()
