import tkinter as tk

from sudoku_game import SudokuGame

from sudoku_solver_frame_widgets import *

LARGE_FONT  = ("Verdana", 20)

class SudokuSolverFrame(tk.Frame):
    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent)

        self.game_engine = SudokuGame()

        #page layout
        self.label_frame_1 = tk.LabelFrame(self ,width=440, height=125,borderwidth=0,highlightthickness=0)
        self.label_frame_1.pack(fill="x", padx = 5, pady=5)

        self.title_label = tk.Label(self.label_frame_1,text = '9x9 sudoku solver',font=LARGE_FONT,padx=10, pady=10)
        self.title_label.pack()

        self.label_frame_2 = tk.LabelFrame(self ,width=440, height=75,borderwidth=0,highlightthickness=0)
        self.label_frame_2.pack(fill="x", padx = 5, pady=5)


        self.about_btn = AboutBtn(self.label_frame_2,self)
        self.about_btn.pack(padx = 5, pady = 5, side = tk.RIGHT)

        self.label_frame_4 = tk.LabelFrame(self ,width=440, height=250,borderwidth=0,highlightthickness=0)
        self.label_frame_4.pack(fill="x", padx = 5, pady=5)

        self.sudoku_grid = SudokuGrid(self.label_frame_4,self.game_engine)

        self.label_frame_5 = tk.LabelFrame(self ,width=440, height=250,borderwidth=0,highlightthickness=0)
        self.label_frame_5.pack(fill="x", padx = 5, pady=5)

        self.solve_btn = SolveBtn( self.label_frame_5,self.game_engine,self.sudoku_grid)
        self.solve_btn.pack(padx = 5, pady=5, side = tk.LEFT)

        self.input_on_web_btn = InputOnWebBtn(self.label_frame_5,self.game_engine)
        self.input_on_web_btn.pack(padx = 5, pady=5, side = tk.RIGHT)

        self.clear_grid_btn = ClearGridBtn(self.label_frame_2,self.game_engine,self.sudoku_grid)
        self.clear_grid_btn.pack(padx = 5, pady = 5, side = tk.LEFT)

