class SudokuGame():
    def __init__(self):
        super().__init__()
        self.sudoku_grid = [
                    [0,0,0,0,0,0,0,0,0],
                    [0,0,0,0,0,0,0,0,0],
                    [0,0,0,0,0,0,0,0,0],
                    [0,0,0,0,0,0,0,0,0],
                    [0,0,0,0,0,0,0,0,0],
                    [0,0,0,0,0,0,0,0,0],
                    [0,0,0,0,0,0,0,0,0],
                    [0,0,0,0,0,0,0,0,0],
                    [0,0,0,0,0,0,0,0,0],
                    ]
    
    
    def update_square(self,j,i,number):
        self.sudoku_grid[j][i] = number

    def clear_sudoku(self):
        for j in range(9):
            for i in range(9):
                self.sudoku_grid[j][i] = 0



    def get_sudoku(self):
        return self.sudoku_grid


