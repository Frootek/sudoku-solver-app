import tkinter as tk
from PIL import ImageTk,Image
import os

SMALL_FONT = ("Verdana", 5)
MEDIUM_FONT = ("Verdana", 10)
MEDIUM_MEDIUM_FONT = ("Verdana", 12)
LARGE_FONT = ("Verdana", 20)

class AboutPage(tk.Frame):
    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent)
        self.label_frame_1 = tk.LabelFrame(self,width=250, height=125,borderwidth=1,highlightthickness=1)
        self.label_frame_1.pack(fill="x", padx = 5, pady=5)



        self.img = Image.open(os.path.abspath("miscellaneous/portrait.png"))
        self.img.thumbnail((150, 150),Image.ANTIALIAS)
        self.img_resized = ImageTk.PhotoImage(self.img)

        self.portrait_img = tk.Label(self.label_frame_1, image = self.img_resized, width = 150, height=150)
        self.portrait_img.pack(side = tk.LEFT, padx=5, pady=5)

        self.information_label = tk.LabelFrame(self.label_frame_1,width=250, height=125, borderwidth=0 ,highlightthickness=0)
        self.information_label.pack(side=tk.LEFT, padx = 5, pady=5)

        self.name_frame = tk.LabelFrame(self.information_label,width=250, height=125,borderwidth=0 ,highlightthickness=0)
        self.name_frame.pack(padx = 5, pady=5,fill='both')

        self.name_label = tk.Label(self.name_frame,text='Name : Marin Jurić',font=MEDIUM_FONT)
        self.name_label.pack(side =tk.LEFT)

        self.age_frame = tk.LabelFrame(self.information_label,width=250, height=125,borderwidth=0 ,highlightthickness=0)
        self.age_frame.pack(padx = 5, pady=5,fill='both')

        self.age_label = tk.Label(self.age_frame,text='Age   : 22',font=MEDIUM_FONT)
        self.age_label.pack(side =tk.LEFT)


        self.country_frame = tk.LabelFrame(self.information_label,width=250, height=125,borderwidth=0 ,highlightthickness=0)
        self.country_frame.pack(padx = 5, pady=5,fill='both')

        self.counter_label = tk.Label(self.country_frame,text='Country : Croatia (Hrvatska) ',font=MEDIUM_FONT)
        self.counter_label.pack(side =tk.LEFT)

        self.faculty_frame = tk.LabelFrame(self.information_label,width=250, height=125,borderwidth=0 ,highlightthickness=0)
        self.faculty_frame.pack(padx = 5, pady=5,fill='both')

        self.faculty = tk.Label(self.faculty_frame,text='University of Zagreb, Faculty of Electrical Engineering and Computing',font=MEDIUM_FONT)
        self.faculty.pack(side =tk.LEFT)

        self.label_frame_2 = tk.LabelFrame(self,width=250, height=325,borderwidth=1,highlightthickness=1, text = "About the App", font=MEDIUM_FONT)
        self.label_frame_2.pack(fill="x", padx = 5, pady=5)

        self.about_app_text = """
        Hello everyone!
        This is a my sudoku 9x9 solver app.
        Search for a sudoku puzzle, input numbers in correct fields and then click solve.
        If sudoku puzzle can be solved solution will be outputed.
        No solution will be outputed if sudoku setup is invalid or if solution can't be found.
        If there are multiple solutions, only one solution will be displayed.
        Clear square by pressing it and then click 'c' or escape button on your keyboard.

        You can also output solution of the puzzle on web sudoku by pressing 'input on web' button. 
        After you click this button your next two left mouse clicks are very  important!
        First left mouse click must be in the upper left corner of the sudoku grid.
        Second left mouse click must be in the lower right corner of the sudoku grid.

        Once you have completed second mouse click program will start to output solution on
        desired sudoku grid.

        Enjoy!

        """
        self.T = tk.Text(self.label_frame_2, font=MEDIUM_MEDIUM_FONT,background="SystemButtonFace",relief=tk.FLAT)
        self.T.pack(fill='both',padx=5,pady=5)
        self.T.insert(tk.END, self.about_app_text)
        self.T.config(state=tk.DISABLED)