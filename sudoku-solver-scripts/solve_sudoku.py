def get_constraints(sudoku_board,j,i):
    possible_numbers = [1,2,3,4,5,6,7,8,9]

    
    #to check if sudoku is valid!
    possible_column_numbers = [1,2,3,4,5,6,7,8,9]
    #column constraint
    for k in range (9):
        if sudoku_board[k][i] in possible_numbers:
            possible_numbers.remove(sudoku_board[k][i])

        if sudoku_board[k][i] in possible_column_numbers:
            possible_column_numbers.remove(sudoku_board[k][i])

        elif sudoku_board[k][i]!=0 :
            raise Exception('Column {} is not valid!'.format(i+1))

    possible_row_numbers = [1,2,3,4,5,6,7,8,9]
    #row constraint
    for k in range (9):
        if sudoku_board[j][k] in possible_numbers:
            possible_numbers.remove(sudoku_board[j][k])

        if sudoku_board[j][k] in possible_row_numbers:
            possible_row_numbers.remove(sudoku_board[j][k])
            
        elif sudoku_board[j][k]!=0 :
            raise Exception('Row {} is not valid!'.format(j+1))



    #mini square constraint

    possible_mini_square_numbers = [1,2,3,4,5,6,7,8,9]
    # (x,y) upper left coordinate of mini square
    x = (i // 3) * 3
    y = (j // 3) * 3

    for m in range (3):
        for n in range(3):
            if sudoku_board[y][x] in possible_numbers:
                possible_numbers.remove(sudoku_board[y][x])
            if sudoku_board[y][x] in possible_mini_square_numbers:
                possible_mini_square_numbers.remove(sudoku_board[y][x])
            elif sudoku_board[y][x]!=0 :
                raise Exception('One of the mini-square is not valid')
            x = x + 1
        x = x - 3
        y = y+1

    return possible_numbers

## will change sudoku_board
## if constraint

def solve_sudoku(sudoku_board):
    #find first empty square

    x,y = 0,0
    
    found = False
    for j in range (9):
        for i in range(9):
            if(sudoku_board[j][i] == 0):
                y = j
                x = i
                found = True
                break
        if found:
            break

    #if no available space sudoku is solved
    if not found:
        return True

    constraints = get_constraints(sudoku_board,y,x)

    for c in constraints:
        sudoku_board[y][x] = c
        solved = solve_sudoku(sudoku_board)
        if solved:
            return True
    sudoku_board[y][x] = 0
    
    return False