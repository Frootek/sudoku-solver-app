# sudoku-solver-app by Frootek

9x9 Sudoku solver app with web output.

## Introduction

Hello everyone!

This is 9x9 sudoku solver.

This repository contains both the script version and executable version of the program.

If you wish to run script version of the program you will need to have the latest versions of following libraries installed

- [Pillow](https://pypi.org/project/Pillow/)
- [pynput](https://pypi.org/project/pynput/)

Once you have installed these libraries you can go ahead and run app.py script.

If you wish to run executable version of the program open sudoku-solver-executable directory and simply
click on app.exe .



## Usage

App contains 9x9 sudoku grid and four buttons.

To fill sudoku grid simply click on sudoku square and press desired number.

To clear sudoku square click on desired sudoku square and press 'c' or escape button on your keyboard.

To clear entire sudoku grid click 'Clear Sudoku'.

Once you have filled all desired squares click 'Solve Sudoku' if sudoku can be solved solution will be outputed.

'Input on web' is used to input sudoku solution on sudoku grid by application.

After you click this button your next two left mouse clicks are very important!

First left mouse click must be in the upper left corner of the sudoku grid.

Second left mouse click must be in the lower right corner of the sudoku grid.

Once you have completed second mouse click program will start to output solution on

desired sudoku grid.


## Demonstration

Start the application and find sudoku puzzle online 
(this puzzle is taken from : [https://sudoku9x9.com/sudoku_daily_9x9.php?level=5](https://sudoku9x9.com/sudoku_daily_9x9.php?level=5))
![Start](readme-pictures/pic_1.JPG)


Input numbers to sudoku squares and then click 'Solve Sudoku' .
![Start](readme-pictures/pic_2.JPG)

If everything is alright solution should be displayed.
![Start](readme-pictures/pic_3.JPG)

Now you can input solution on web.
Click on 'Input on web' then click on upper left corner and after that on lower right corner of sudoku board.
![Start](readme-pictures/pic_4.JPG)


Sit back and enjoy as program inputs solution on desired sudoku board.
![Start](readme-pictures/pic_5.JPG)


## License
This software is licensed under the [MIT License](LICENSE).